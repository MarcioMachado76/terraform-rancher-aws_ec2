# Terraform configuration

terraform {
  required_version = "~> 0.14"

  required_providers {
    aws = {
      source = "hashicorp/aws"

      version = "3.33.0"

    }
  }
  backend "s3" {
    bucket  = "devopscloud-tfstate"
    key     = "rancher/terraform.state"
    region  = "us-east-1"
    profile = "terraform"
  }
}

provider "aws" {
  region  = var.region
  profile = var.profile_aws

}


