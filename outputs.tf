output "sg-rancher_id" {
  value = aws_security_group.rancher.id

}
output "id_rancher-ec2" {
  description = "ID of the EC2 instance"
  value       = aws_instance.rancher.id
}

output "public_ip-rancher" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.rancher.public_ip
}

output "id_k8s-1-ec2" {
  description = "ID of the EC2 instance"
  value       = aws_instance.k8s-1.id
}

output "public_ip-k8s-1" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.k8s-1.public_ip
}

output "id_k8s-2-ec2" {
  description = "ID of the EC2 instance"
  value       = aws_instance.k8s-2.id
}

output "public_ip-k8s-2" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.k8s-2.public_ip
}

output "id_k8s-3-ec2" {
  description = "ID of the EC2 instance"
  value       = aws_instance.k8s-3.id
}

output "public_ip-k8s-3" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.k8s-3.public_ip
}

 output "name-servers" {
   description = "List nameservers"
   value = aws_route53_zone.devops.name_servers   
 }