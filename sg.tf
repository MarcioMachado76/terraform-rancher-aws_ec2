##################
## Security Group
##################

resource "aws_security_group" "rancher" {
  name        = "rancher-sg"
  description = "Allow Public inbound traffic"
  vpc_id      = aws_vpc.vpc_devops.id

  ###############
  # Inbound  TCP
  ###############
  dynamic "ingress" {
    for_each = var.sg-tcp-ingress
    content {
      description = ingress.value["description"]
      from_port   = ingress.key
      to_port     = ingress.key
      protocol    = "tcp"
      cidr_blocks = ingress.value["cidr_blocks"]
    }
  }

  ingress {
    from_port = 2379
    to_port   = 2380
    protocol  = "tcp"
    self      = true
    description = "etcd client requests/etcd peer communication"
  }

  ingress {
    from_port = 6783
    to_port   = 6783
    protocol  = "tcp"
    self      = true
    description = "Weave Port"
  }

  ingress {
    from_port = 10250
    to_port   = 10252
    protocol  = "tcp"
    self      = true
    description = "Kubelet API/kube-scheduler/kube-controller-manager "
  }

  ingress {
    from_port = 10256
    to_port   = 10256
    protocol  = "tcp"
    self      = true
    description = "node health check"
  }

  ingress {
    from_port   = 30000
    to_port     = 32767
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "NodePort port range"
  }

  ###############
  # Inbound  UDP
  ###############
  dynamic "ingress" {
    for_each = var.sg-udp-ingress
    content {
      description = ingress.value["description"]
      from_port   = ingress.key
      to_port     = ingress.key
      protocol    = "udp"
      self        = ingress.value["self"]
    }
  }


  ingress {
    from_port   = 30000
    to_port     = 32767
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "NodePort port range"    
  }

  ####################
  # Outbound  TCP/UDP
  ####################
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Rancher Server"
  }
}