###############
###  AWS VPC
###############

resource "aws_vpc" "vpc_devops" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true

  tags = {
    Name = var.vpc_name
  }
}
