variable "region" {
  description = "Região US-EAST-1, Norte da Virgínia"
  type        = string
  default     = "us-east-1"
}

variable "profile_aws" {
  description = "Profile conta aws"
  type        = string
  default     = "default"

}

variable "vpc_name" {
  description = "Nome da VPC"
  type        = string
  default     = "vpc_devops"
}

variable "vpc_cidr" {
  description = "CIDR Blocks da VPC"
  type        = string
  default     = "10.50.0.0/16"
}

variable "vpc_azs" {
  description = "Zonas de disponibilidade da VPC"
  type        = list(string)
  default     = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d"]
}

variable "private_subnets" {
  description = "Subnets privadas da VPC"
  type        = list(string)
  default     = ["10.50.1.0/24", "10.50.2.0/24", "10.50.3.0/24", "10.50.1.0/24"]
}

variable "public_subnets" {
  description = "Subnets publicas da VPC"
  type        = list(string)
  default     = ["10.50.5.0/24", "10.50.6.0/24", "10.50.7.0/24", "10.50.8.0/24"]
}

variable "sg-tcp-ingress" {
  type = map(object({ description = string, cidr_blocks = list(string) }))
  default = {
    22   = { description = "Inbound para SSH", cidr_blocks = ["0.0.0.0/0"] }
    80   = { description = "Inbound para HTTP", cidr_blocks = ["0.0.0.0/0"] }
    8080 = { description = "Inbound para HTTP", cidr_blocks = ["0.0.0.0/0"] }
    443  = { description = "Inbound para HTTPS", cidr_blocks = ["0.0.0.0/0"] }
    6443 = { description = "Inbound rancher docker", cidr_blocks = ["0.0.0.0/0"] }
    2376 = { description = "Node driver Docker daemon TLS port", cidr_blocks = ["0.0.0.0/0"] }
  }
}

variable "sg-udp-ingress" {
  type = map(object({ description = string, self = bool }))
  default = {
    8472 = { description = "Canal/Flannel VXLAN overlay networking", self = true }
    2376 = { description = "Flannel VXLAN overlay networking on Windows cluster", self = true }
  }
}

variable "key_pair" {
  type        = string
  description = "Chave para se conectar ssh"
  default     = "<my key pair>"

}

variable "ami" {
  type        = string
  default     = "ami-03d315ad33b9d49c4"
  description = "UBUNTU 20.04"
}

variable "type" {
  type        = string
  default     = "t2.micro"
  description = "Type instance"
}

variable "dns" {
  type        = string
  description = "Nome do domínio a ser registrado no route53"
  default     = "<my.domain.com>"
}

variable "rancher" {
  type        = string
  description = "caminho do script de instalação"
  default     = "scripts/rancher.sh"
}

variable "k8s" {
  type        = string
  description = "caminho do script de instalação"
  default     = "scripts/k8s.sh"
}
