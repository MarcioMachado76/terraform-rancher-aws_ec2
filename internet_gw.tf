######################
##  Internet Gateway
######################

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc_devops.id

  tags = {
    Name = "igw-devops"
  }
}