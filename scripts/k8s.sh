#!/bin/bash
# Update and upgrade system
apt-get update 
apt-get upgrade -y
hostnamectl set-hostname k8s

# Docker install
apt install docker.io -y
usermod -aG docker ubuntu
