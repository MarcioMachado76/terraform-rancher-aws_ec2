#!/bin/bash
# Update and upgrade system
apt-get update && apt-get -y upgrade
hostnamectl set-hostname rancher 
apt install -y git
apt  install docker.io -y
usermod -aG docker ubuntu 
curl -L "https://github.com/docker/compose/releases/download/1.28.6/docker-compose-$(uname -s)-$(uname -m)" \ 
-o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

#docker run -d --name rancher --restart=unless-stopped -v /opt/rancher:/var/lib/rancher -p 80:80 -p 443:443 --privileged rancher/rancher:latest