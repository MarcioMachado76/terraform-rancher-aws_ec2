##################
## Public subnets
##################

resource "aws_subnet" "public_a" {
  vpc_id                  = aws_vpc.vpc_devops.id
  cidr_block              = "10.50.0.0/24"
  availability_zone       = "${var.region}a"
  map_public_ip_on_launch = true

  tags = {
    Name = "Public 1a"
  }
}

resource "aws_subnet" "public_b" {
  vpc_id                  = aws_vpc.vpc_devops.id
  cidr_block              = "10.50.1.0/24"
  availability_zone       = "${var.region}b"
  map_public_ip_on_launch = true

  tags = {
    Name = "Public 1b"
  }
}

resource "aws_subnet" "public_c" {
  vpc_id                  = aws_vpc.vpc_devops.id
  cidr_block              = "10.50.2.0/24"
  availability_zone       = "${var.region}c"
  map_public_ip_on_launch = true

  tags = {
    Name = "Public 1c"
  }
}

resource "aws_subnet" "public_d" {
  vpc_id                  = aws_vpc.vpc_devops.id
  cidr_block              = "10.50.3.0/24"
  availability_zone       = "${var.region}d"
  map_public_ip_on_launch = true

  tags = {
    Name = "Public 1d"
  }
}

###################
## Private Subnets
###################

resource "aws_subnet" "private_a" {
  vpc_id            = aws_vpc.vpc_devops.id
  cidr_block        = "10.50.4.0/24"
  availability_zone = "${var.region}a"

  tags = {
    Name = "Private 1a"
  }
}

resource "aws_subnet" "private_b" {
  vpc_id            = aws_vpc.vpc_devops.id
  cidr_block        = "10.50.5.0/24"
  availability_zone = "${var.region}b"

  tags = {
    Name = "Private 1b"
  }
}

resource "aws_subnet" "private_c" {
  vpc_id            = aws_vpc.vpc_devops.id
  cidr_block        = "10.50.6.0/24"
  availability_zone = "${var.region}c"

  tags = {
    Name = "Private 1c"
  }
}

resource "aws_subnet" "private_d" {
  vpc_id            = aws_vpc.vpc_devops.id
  cidr_block        = "10.50.7.0/24"
  availability_zone = "${var.region}d"

  tags = {
    Name = "Private 1d"
  }
}