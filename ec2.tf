########################################
## AWS Instance
## ubuntu 20.04 - ami-03d315ad33b9d49c4 
########################################

resource "aws_instance" "rancher" {
  ami             = var.ami
  instance_type   = var.type
  user_data       = filebase64(var.rancher)
  key_name        = var.key_pair
  security_groups = [aws_security_group.rancher.id]
  subnet_id       = aws_subnet.public_a.id

  tags = {
    Name = "rancher-server"
  }
}

resource "aws_instance" "k8s-1" {
  ami             = var.ami
  instance_type   = var.type
  user_data       = filebase64(var.k8s)
  key_name        = var.key_pair
  security_groups = [aws_security_group.rancher.id]
  subnet_id       = aws_subnet.public_b.id

  tags = {
    Name = "k8s-1"
  }
}

resource "aws_instance" "k8s-2" {
  ami             = var.ami
  instance_type   = var.type
  user_data       = filebase64(var.k8s)
  key_name        = var.key_pair
  security_groups = [aws_security_group.rancher.id]
  subnet_id       = aws_subnet.public_c.id

  tags = {
    Name = "k8s-2"
  }
}

resource "aws_instance" "k8s-3" {
  ami             = var.ami
  instance_type   = var.type
  user_data       = filebase64(var.k8s)
  key_name        = var.key_pair
  security_groups = [aws_security_group.rancher.id]
  subnet_id       = aws_subnet.public_d.id

  tags = {
    Name = "k8s-3"
  }
}