##########################
## Criando Zona hospedada 
##########################

resource "aws_route53_zone" "devops" {
  name = var.dns
}

############################
## Criando criando registro 
############################
resource "aws_route53_record" "rancher" {
  zone_id = aws_route53_zone.devops.zone_id
  name    = "rancher.${var.dns}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.rancher.public_ip]
}

#############################
## Criando registro wildcard
#############################

resource "aws_route53_record" "cluster" {
  zone_id = aws_route53_zone.devops.zone_id
  name    = "*.${var.dns}"
  type    = "A"
  ttl     = "300"
  records = [
    aws_instance.k8s-1.public_ip,
    aws_instance.k8s-2.public_ip,
    aws_instance.k8s-3.public_ip,
  ]
}
